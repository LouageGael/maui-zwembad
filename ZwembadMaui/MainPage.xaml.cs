﻿namespace ZwembadMaui;

public partial class MainPage : ContentPage
{
    private List<Zwembad> lijstZwembad = new List<Zwembad>();
    private Zwembad zwembad = null;
    public MainPage()
	{
		InitializeComponent();
	}
    private void ClearTextboxes()
    {
        txtDiepte.Text = String.Empty;
        txtLengte.Text = String.Empty;
        txtBreedte.Text = String.Empty;
        txtRandafstand.Text = String.Empty;
    }
    // listview selecteditem
    private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
    {
        zwembad = lvAantalLiter.SelectedItem as Zwembad;
        txtBreedte.Text = zwembad.Breedte.ToString("F2");
        txtDiepte.Text = zwembad.Diepte.ToString("F2");
        txtLengte.Text = zwembad.Lengte.ToString("F2");
        txtRandafstand.Text = zwembad.Randafstand.ToString("F2");
        
    }
    private void OnBtnEditClicked(object sender, EventArgs e)
    {
        if (lvAantalLiter.SelectedItem != null && zwembad != null)
        {
           var selectedZwembad = lijstZwembad.First(x => x.Id == zwembad.Id);
            selectedZwembad.Diepte = Convert.ToDouble(txtDiepte.Text);
            selectedZwembad.Lengte = Convert.ToDouble(txtLengte.Text);
            selectedZwembad.Breedte = Convert.ToDouble(txtBreedte.Text);
            selectedZwembad.Randafstand = Convert.ToDouble(txtRandafstand.Text);
            lvAantalLiter.ItemsSource = null;
            lvAantalLiter.ItemsSource = lijstZwembad;
            ClearTextboxes();
        }
    }
    private void OnBtnDeleteClicked(object sender, EventArgs e)
    {
        if(lvAantalLiter.SelectedItem != null && zwembad != null)
        {
            lijstZwembad.Remove(zwembad);
            lvAantalLiter.ItemsSource = null;
            lvAantalLiter.ItemsSource = lijstZwembad;
        }
    }
    private void OnBtnBerekenClicked(object sender, EventArgs e)
	{
		if(!string.IsNullOrEmpty(txtBreedte.Text) || !string.IsNullOrEmpty(txtDiepte.Text) ||
			!string.IsNullOrEmpty(txtLengte.Text) || !string.IsNullOrEmpty(txtRandafstand.Text))
        {
          
            if (double.TryParse(txtBreedte.Text , out double breedte) &&
                double.TryParse(txtDiepte.Text, out double diepte) &&
                double.TryParse(txtLengte.Text, out double lengte) &&
					double.TryParse(txtRandafstand.Text, out double randafstand))
            {
                
                lijstZwembad.Add(new Zwembad()
                {
                    Breedte = breedte,
                    Diepte = diepte,
                    Lengte = lengte,
                    Randafstand = randafstand,
                    Id =  Guid.NewGuid()
                });
                lvAantalLiter.ItemsSource = null;
                lvAantalLiter.ItemsSource = lijstZwembad;
                ClearTextboxes();
                lblFouteWaarde.BackgroundColor = Color.FromArgb("#FFFFFF");
            } else
            {
                lblFouteWaarde.Text = "Alle tekstvelden moeten numeriek zijn.";
                lblFouteWaarde.BackgroundColor = Color.FromArgb("#FF0000");
            }
        } else
        {
            lblFouteWaarde.Text = "Niet alle tekstvelden zijn ingevuld.";
            lblFouteWaarde.BackgroundColor = Color.FromArgb("#FF0000");
        }
	}
}
class Zwembad
{
    private Guid _id;
	private double _breedte;
	private double _lengte;
	private double _randafstand;
	private double _diepte;
    

    public double Breedte
    { 
        get => _breedte; 
        set
        {
            if(value < 0)
            {
                _breedte = 0;
            } else
            {
                _breedte = value;
            }
        }
    }
    public double Lengte 
    {
        get => _lengte;
        set
        {
            if (value < 0)
            {
                _lengte = 0;
            }
            else
            {
                _lengte = value;
            }
        }
    }
    public double Randafstand
    { 
        get => _randafstand;
        set
        {
            if (value < 0)
            {
                _randafstand = 0;
            }
            else
            {
                _randafstand = value;
            }
        }
    }
    public double Diepte
    { 
        get => _diepte;
        set
        {
            if (value < 0)
            {
                _diepte = 0;
            }
            else
            {
                _diepte = value;
            }
        }
    }
    public  double LiterWater => Lengte * Breedte * (Diepte-Randafstand) * 1000;

    public Guid Id { get => _id; set => _id = value; }
}

